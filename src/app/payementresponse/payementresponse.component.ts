import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderService } from '../services/order.service';
import swal from 'sweetalert2';
import { CartService } from '../services/cart.service';

@Component({
  selector: 'app-payementresponse',
  templateUrl: './payementresponse.component.html',
  styleUrls: ['./payementresponse.component.scss']
})
export class PayementresponseComponent implements OnInit {

  constructor(public orderservice:OrderService,private activatedRoute: ActivatedRoute,private router:Router,
    public cartService:CartService) { 
    var successFlag=this.activatedRoute.snapshot.params.successflag;
    console.log(successFlag)
    if(successFlag==1){
      sessionStorage.removeItem('cartItems')
      this.router.navigateByUrl('order-confirmation')
    }else{
      swal.fire({
        title: 'Payment Failed',
        text: ' Something Went wrong!',
        icon: 'error',
        confirmButtonText: 'OK',
        allowOutsideClick: false
      })
      let data=JSON.parse(sessionStorage.getItem('cartItems'));
      this.cartService.cart['menuItems'] = data.menuItems;
      this.cartService.cart['package'] = data.package;
      this.cartService.cart['combo'] = data.combo;    
        this.router.navigateByUrl('place-order')

    }
  }

  ngOnInit(): void {
  
  }
  updateOrderPaymentStatus(){
    let obj=
    {
        "OrderPaymentDetails":
      {
        "orderPaymentId": 1,
        "successFlag": 1,
        "processedOn": "11/23/2020 7:11 PM",
        "transactionId": "12abc543"
      }
    }
    try {
      this.orderservice.updateOrderPaymentStatus(obj).subscribe(res=>{
        console.log(res)
        let data:any=res;
      })
    } catch (error) {
      
    }
  }

}
