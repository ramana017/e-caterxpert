import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayementresponseComponent } from './payementresponse.component';

describe('PayementresponseComponent', () => {
  let component: PayementresponseComponent;
  let fixture: ComponentFixture<PayementresponseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayementresponseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayementresponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
