import { Component, DoCheck, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/services/auth.service';
import { CartService } from 'src/app/services/cart.service';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, DoCheck {
  public loginres = false;
  public loginresponseData;
  public item:any;
  logo=false;

  constructor(private router: Router,
              private cartService: CartService,
              public authService: AuthService,
              private orderService:OrderService,
              private spinner:NgxSpinnerService) { }

  ngOnInit(): void {
    // this.getCatererLogo()

  }

  ngDoCheck(){
    if(JSON.parse(sessionStorage.getItem('loginResponse'))){
      this.loginresponseData = JSON.parse(sessionStorage.getItem('loginResponse'));
      if(this.loginresponseData.AuthenticateUser.userId!==0)
      {
        this.loginres = true;
      }else{
        this.loginres = false;

      }
    }else {
      this.loginres = false;
    }
  }

  public logOut(){
    let sessionData=JSON.parse(sessionStorage.getItem('loginResponse'));
this.spinner.show();
    try {
      console.log(sessionData.AuthenticateUser.sessionId)
      this.authService.logout(sessionData.AuthenticateUser.sessionId).subscribe(
        res=>{
          this.spinner.hide();
           console.log(res)
           let data:any=res;
           if(data.validateFlag==1){
            sessionStorage.removeItem('loginResponse');
            this.authService.redirectToHome();
            this.cartService.cart.combo  = [];
            this.cartService.cart.menuItems  = [];
            this.cartService.cart.package  = [];
           }
        },
        error=>{
          this.spinner.hide();

        }
      )
    } catch (error) {
      
    }
  }

  loginFromHome(){
    this.cartService.loginFromHome.next(true);
    // this.router.navigate(['login']);
    this.router.navigateByUrl('login')

  }
  // public getCatererLogo(){
  //   let value: string = sessionStorage.getItem('randomvalue');
  //   try {
  //     this.orderService.getBrandingDetails(value.split('=')[1]).subscribe(
  //       res=>{
  //         this.item=res;
  //         this.logo=this.item.brandingDetails[0].blobcontent?true:false;
  //         console.log(res);
  //         console.log(this.logo)
  //         // console.log(Object.keys(this.item.CaterLogo).length,this.item)
  //       }
  //     )
  //   } catch (error) {
      
  //   }
  // }
 

}
