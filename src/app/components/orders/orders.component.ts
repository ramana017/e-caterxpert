import { Component, OnInit, OnChanges } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
})
export class OrdersComponent implements OnInit {
  public cartItems;
  public subTotal;
  public packageItems;
  public comboItems;
  public repeatOrderData = false;
  constructor(public cartService: CartService,
    private authService: AuthService,
    private router: Router,
    private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.cartItems = this.cartService.cart.menuItems;
    this.packageItems = this.cartService.cart.package;
    this.comboItems = this.cartService.cart.combo;
    this.packageItems.forEach((x) => {
      if (x.PackageDetails.packageItemLevelPriceFlag == 1) {
        x.subMenu = x.PackageItems.filter(y => y.pkgItemQty > 0).map(z => z.pkgItemname).toString();
      }
    })
    console.log(this.packageItems);
    this.calcSubTotal();
    if (JSON.parse(sessionStorage.getItem('loginResponse'))) {
      let data: any = JSON.parse(sessionStorage.getItem('loginResponse'));
      data.AuthenticateUser.userId != 0 ? this.repeatOrderData = true : this.repeatOrderData = false;
    };
    console.log(this.packageItems);
  }
  public deleteItem(i) {
    this.cartService.cart.menuItems.splice(i, 1);
    this.calcSubTotal();

  }
  public deleteCombo(i) {
    this.cartService.cart.combo.splice(i, 1);
    this.calcSubTotal();

  }
  public deletePackage(i) {
    this.cartService.cart.package.splice(i, 1);
    this.calcSubTotal();

  }
  changeQuantity(i, $event) {
    console.log(this.cartService.cart.menuItems)

    console.log(+$event.target.value)
    this.cartService.cart.menuItems[i].quantity = +$event.target.value;

    this.calcSubTotal();


  }

  calcSubTotal() {
    let subTotal = [];
    this.cartService.cart.menuItems.forEach((x) => {
      subTotal.push(x.price * x.quantity);
    });
    this.cartService.cart.package.forEach((x, i) => {
      console.log("On Calculate", x);
      if (x.PackageDetails.packageItemLevelPriceFlag == 1) {
        subTotal.push(x.PackageDetails.packageCost);
      } else {
        subTotal.push(+x.PackageDetails.PkgQty * +x.PackageDetails.packageCost);
      }
    });
    this.cartService.cart.combo.forEach((x) => {
      subTotal.push(+x.ComboDetails.comboQty * +x.ComboDetails.comboCost)
    });
    this.subTotal = subTotal.reduce((a, b) => a + b, 0);
  }

  checkOutGuest() {


    if (this.cartService.cart.combo.length > 0 || this.cartService.cart.menuItems.length > 0 || this.cartService.cart.package.length > 0) {

      this.spinner.show();
      this.authService.guestUser().subscribe((res) => {
        sessionStorage.setItem('loginResponse', JSON.stringify(res))
        this.router.navigate(['order-details']);
      }, err => { this.spinner.hide(); }, () => { this.spinner.hide(); });
    } else {
      Swal.fire({
        title: 'Cart is empty',
        text: ' Please add items and proceed',
        imageUrl: 'assets/images/cart.png',
        imageWidth: 100,
        imageHeight: 100,
        // icon: 'error',
        confirmButtonText: 'OK',
        allowOutsideClick: false
      }).then(ok => {
        console.log("yes")
        this.authService.redirectToHome();
      })
    }

  }


  continue() {
    if (this.cartService.cart.combo.length > 0 || this.cartService.cart.menuItems.length > 0 || this.cartService.cart.package.length > 0) {
      this.spinner.show();
      this.authService.guestUser().subscribe((res) => {
        let tempres = JSON.parse(sessionStorage.getItem('loginResponse'))
        tempres.AuthenticateUser.newOrderId = res.AuthenticateUser.newOrderId;
        console.log(tempres)
        sessionStorage.setItem('loginResponse', JSON.stringify(tempres))
        this.router.navigate(['order-details']);
      }, err => { this.spinner.hide(); }, () => { this.spinner.hide(); });
    } else {
      Swal.fire({
        title: 'Cart is empty',
        text: ' Please add items and proceed',
        imageUrl: 'assets/images/cart.png',
        imageWidth: 100,
        imageHeight: 100,
        // icon: 'error',
        confirmButtonText: 'OK',
        allowOutsideClick: false
      }).then(ok => {
        console.log("yes")
        this.authService.redirectToHome();
      })
    }

  }

  checkOutUser() {
    if (this.cartService.cart.combo.length > 0 || this.cartService.cart.menuItems.length > 0 || this.cartService.cart.package.length > 0) {
      this.cartService.loginFromHome.next(null);
      this.router.navigate(['login']);
    } else {
      Swal.fire({
        title: 'Cart is empty',
        text: ' Please add items and proceed',
        imageUrl: 'assets/images/cart.png',
        imageWidth: 100,
        imageHeight: 100,
        // icon: 'error',
        confirmButtonText: 'OK',
        allowOutsideClick: false
      }).then(ok => {
        console.log("yes")
        this.authService.redirectToHome();
      })
    }

  }

  addMore() {
    this.authService.redirectToHome();
  }

}
