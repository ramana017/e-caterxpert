import { Component, OnInit, TemplateRef } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { OrderService } from 'src/app/services/order.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-my-orders-list',
  templateUrl: './my-orders-list.component.html',
  styleUrls: ['./my-orders-list.component.scss']
})
export class MyOrdersListComponent implements OnInit {
  public ordersList;
  public p = 1;
  public loginres = JSON.parse(sessionStorage.getItem('loginResponse'));
  modalRef?: BsModalRef;


  constructor(private modalService: BsModalService, private orderService: OrderService,
    private spinner: NgxSpinnerService,
    private cartService: CartService,
    private authService: AuthService,
    private router: Router) { }

  ngOnInit(): void {
    this.getOrderList(this.loginres.AuthenticateUser.userId);
  }

  public getOrderList(id): void {
    this.spinner.show();
    this.orderService.getPreviousOrders(id)
      .subscribe((data) => {
        this.ordersList = data;
      }, err => { this.spinner.hide(); }, () => { this.spinner.hide(); });
  }

  public changeCount(event): void {
    this.p = 1;
  }

  public repeatOrder(orderNumber): void {
    this.spinner.show();
    this.orderService.repeatOrder(orderNumber).subscribe((res) => {

      let orderDetails = res.OrderDetails;
      let obj = {
        "orderNumber": orderDetails.OrderName,
        "orderName": orderDetails.OrderName,
        "orderType": orderDetails.OrderType,
        "date": null,
        "orderEndDate": null,
        "eventStartDate": null,
        "eventEndDate": null,
        "name": orderDetails.GuestUserName,
        "phone": orderDetails.HomePhone,
        "email": orderDetails.email,
        "street": orderDetails.street,
        "suite": orderDetails.email,
        "city": orderDetails.city,
        "state": orderDetails.state,
        "zipCode": orderDetails.zip,
        "statename": "",
        "notes": orderDetails.Notes,
        "guestCount": "",
        "discountCode": "",
        "discountFlat": 0,
        "discountPercentage": 0,
        "billingCode": "",
        "address": orderDetails.street
      }
      sessionStorage.setItem('orderDetails', JSON.stringify(obj));

      this.cartService.cart['menuItems'] = res.menuItems;
      this.cartService.cart['package'] = res.package;
      this.cartService.cart['combo'] = res.combo;




      console.log(this.cartService.cart);
      this.cartService.cart.package.map(x => {
        console.log("cart package", x)

        if (x.PackageDetails.packageItemLevelPriceFlag == 1) {
          console.log("Packageleval flag")
          let totalPrice = 0;
          x.PackageItems.map(y => {
            if (y.pkgItemQty != 0) {
              totalPrice += (+y.pkgItemPrice) * (+y.pkgItemQty);
            }
          })
          console.log("Package cost", totalPrice)
          x.PackageDetails.packageCost = totalPrice;
        }
      })
      this.authService.redirectToHome();
    }, err => { this.spinner.hide(); }, () => { this.spinner.hide(); });
  }

  home() {
    this.authService.redirectToHome();
  }
  public menuItems = [];
  public packageItems = [];
  public comboItems = [];
  public subTotal;

  public orderNum;

  public viewOrderDetails(orderNumber,template): void {
    this.orderNum=orderNumber;
    this.spinner.show();
    this.orderService.repeatOrder(orderNumber).subscribe((res) => {
      this.menuItems = res.menuItems;
      this.packageItems = res.package;
      this.comboItems = res.combo;
      console.log(res)
      this.calcSubTotal();
      this.modalRef = this.modalService.show(template,{class:'modal-xl review-orders-dialog'});
    }, err => { this.spinner.hide(); }, () => { this.spinner.hide(); });
  }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  calcSubTotal() {
    let subTotal = [];
    this.menuItems.forEach((x) => {
      subTotal.push(x.price * x.quantity);
    });
    this.packageItems.forEach((x, i) => {
      console.log("On Calculate", x);
      if (x.PackageDetails.packageItemLevelPriceFlag == 1) {
        let total=0;
        x.PackageItems.map(y=>{
          total+=(+y.pkgItemPrice)*(+y.pkgItemQty);
          subTotal.push(y.pkgItemPrice*y.pkgItemQty);
        })
        console.log(total);
        x.PackageDetails.packageCost=total;

      } else {
        subTotal.push(+x.PackageDetails.PkgQty * +x.PackageDetails.packageCost);
      }
    });
    this.comboItems.forEach((x) => {
      subTotal.push(+x.ComboDetails.comboQty * +x.ComboDetails.comboCost)
    });
    this.subTotal = subTotal.reduce((a, b) => a + b, 0);
  }
}
