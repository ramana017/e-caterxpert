import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-order-confirmation',
  templateUrl: './order-confirmation.component.html',
  styleUrls: ['./order-confirmation.component.scss']
})
export class OrderConfirmationComponent implements OnInit, AfterViewInit {
  public loginres = JSON.parse(sessionStorage.getItem('loginResponse'));

  public totalAmount;


  //vr fixed
  public orderDetails = JSON.parse(sessionStorage.getItem('orderDetails'))
  constructor(private router: Router,
    private cartService: CartService,
    private authService: AuthService) { }

  ngOnInit(): void {
    sessionStorage.removeItem('cartItems')
    sessionStorage.removeItem('orderDetails');
    this.totalAmount = sessionStorage.getItem('totalvalues');
    // console.log(this.totalAmount.toFixed(2))
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      if (this.loginres.AuthenticateUser.userId !== 0) {
        this.cartService.clearCartItems();
        this.router.navigate(['orders-list']);
      } else {
        this.cartService.clearCartItems();
        this.authService.redirectToHome();
      }
      let userlogin = JSON.parse(sessionStorage.getItem('loginResponse'));
      if (userlogin.AuthenticateUser.userId == 0) {

        sessionStorage.removeItem('loginResponse');
      }
    }, 5000);
  }

}
