import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BreakfastPopupComponent } from './breakfast-popup.component';

describe('BreakfastPopupComponent', () => {
  let component: BreakfastPopupComponent;
  let fixture: ComponentFixture<BreakfastPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BreakfastPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreakfastPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
