import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { CartService } from 'src/app/services/cart.service';
import { ValidationAlertDialogComponent } from 'src/app/shared/dialogs/validation-alert-dialog/validation-alert-dialog.component';
import Swal from 'sweetalert2';
declare var $: any;
const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 5000,
  timerProgressBar: true,
  didOpen: (toast) => {

    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  }
})
@Component({
  selector: 'app-item-leval-package',
  templateUrl: './item-leval-package.component.html',
  styleUrls: ['./item-leval-package.component.scss']
})
export class ItemLevalPackageComponent implements OnInit {

  public totalPrice: number = 0;
  public totalQuantity: number = 1;
  public packageDetails;
  public packageItems = [];



  public showSummary = false;
  public product; // coming from component reciepe component as intial value

  constructor(private spinner: NgxSpinnerService,
    private cartService: CartService, private modalRef: BsModalRef, private modalService: BsModalService) { }

  ngOnInit(): void {
    console.log('Cart Items', this.cartService.cart);
    this.getComboItems(this.product.itemId);
  }

  public getComboItems(itemId): void {
    this.spinner.show();
    this.cartService.getPackage(itemId).subscribe((data) => {
      console.log(data)

      this.packageDetails = data.PackageDetails;
      this.packageItems = data.PackageItems;


      this.assignQuantity();
    },
      err => {
        this.spinner.hide();
        Toast.fire({
          icon: 'error',
          title: 'Failed to load Items!'
        })
      }, () => { this.spinner.hide(); });
  }

  /**
   * methhod is used to increase or decrease the quantity of each individual item
   */
  public changeItemQuantity(index, flag) {
    flag == 'plus' ? this.packageItems[index].pkgItemQty = this.packageItems[index].pkgItemQty + 1 : this.packageItems[index].pkgItemQty = this.packageItems[index].pkgItemQty - 1;

    let totalPrice = 0;
    this.itemsQuantity = 0;
    this.packageItems.map(x => {
      this.itemsQuantity += x.pkgItemQty
      if (x.pkgItemQty != 0) {
        totalPrice += (+x.pkgItemPrice) * (+x.pkgItemQty);
        this.totalPrice = totalPrice;
      }
    })


  }






  closeModal() {
    this.modalRef.hide();
  }

  /**
   * function : assignQuantity
   * purpose : if the selected item already in the cart we are assigning the quantity on load
   */
  assignQuantity() {
    if (this.cartService.cart.package.length) {
      // this.cartService.cart.package.find(
      //   ({ PackageDetails, PackageItems }) => {
      //     if (PackageDetails.packageId === this.packageData.PackageDetails.packageId) {
      //       this.packageData['PackageDetails'] = JSON.parse(JSON.stringify(PackageDetails));
      //       this.packageData['PackageItems'] = JSON.parse(JSON.stringify(PackageItems));
      //       this.packageData.PackageItems.forEach((x, i) => {
      //         x.pkgItemQty = x.pkgItemQty;
      //       });
      //     }
      //   }
      // );
      this.cartService.cart.package.map(x => {
        if (x.PackageDetails.EcateringItemId == this.product.itemId) {
          console.log('$$$$$$$ found')
          this.packageDetails = x.PackageDetails;
          this.packageItems = x.PackageItems;

        }
      })
    }

    this.packageItems.map(x => {
      this.itemsQuantity += x.pkgItemQty
      if (x.pkgItemQty != 0) {
        this.totalPrice += (+x.pkgItemPrice) * (+x.pkgItemQty);
      }
    })

  }


  public itemsQuantity = 0;
  public onSave() {
    if (this.itemsQuantity == 0) {
      this.modalService.show(ValidationAlertDialogComponent, {
        class: 'modal-dialog-custom ',
        initialState: { message: `Add at least one item before save ` },
        keyboard: false,
      });
    } else {
      this.showSummary = true;

    }

  }



  changeTotalQty(flag) {
    this.totalQuantity = flag == "plus" ? this.totalQuantity + 1 : this.totalQuantity - 1;
  }



  public setData() {
    let data =
    {
      "PackageDetails": {
        "EcateringItemId": this.packageDetails.EcateringItemId,
        "packageId": this.packageDetails.packageId,
        "pkgMaxItems": this.packageDetails.pkgMaxItems,
        "pkgMinOrderQty": this.packageDetails.pkgMinOrderQty,
        "packageType": this.packageDetails.packageType,
        "PackageComboFlag": this.packageDetails.PackageComboFlag,
        "packageItemLevelPriceFlag": this.packageDetails.packageItemLevelPriceFlag,
        "CaterxpertItemId": this.packageDetails.CaterxpertItemId,
        "packageCost": this.totalPrice,
        "unitId": this.packageDetails.unitId,
        "pkgMaxSelectItemQty": this.packageDetails.pkgMaxSelectItemQty,
        "packageName": this.packageDetails.packageName,
        "PkgDescription": this.packageDetails.PkgDescription,
        "pkgMinItems": this.packageDetails.pkgMinItems,
        "PkgQty": this.itemsQuantity,
        "pkgServiceId": this.packageDetails.pkgServiceId,
      },
      "PackageItems": this.packageItems.map(x => {
        let obj = {
          "pkgEcateringItemId": x.pkgEcateringItemId,
          "pkgCaterXpertItemId": x.pkgCaterXpertItemId,
          "pkgItemPrice": x.pkgItemPrice,
          "pkgItemname": x.pkgItemname,
          "pkgItemQty": x.pkgItemQty,
          "pkgItemId": x.pkgItemId,
          "pkgServiceId": x.pkgServiceId,
        }
        return obj;

      })
    }
    let checkDataExist = false;
    this.cartService.cart.package.forEach((x, i) => {

      if (this.packageDetails.packageId === x.PackageDetails.packageId) {
        checkDataExist = true;
        this.cartService.cart.package[i] = data;
      }
    });
    if (!checkDataExist) {
      this.cartService.cart.package.push(data);
    }
    this.modalService._hideModal(this.modalService.getModalsCount());

  }




}

