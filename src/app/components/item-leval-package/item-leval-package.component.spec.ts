import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemLevalPackageComponent } from './item-leval-package.component';

describe('ItemLevalPackageComponent', () => {
  let component: ItemLevalPackageComponent;
  let fixture: ComponentFixture<ItemLevalPackageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemLevalPackageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemLevalPackageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
