import {
  Component,
  OnInit,
  DoCheck,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnDestroy,
} from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { debounceTime, map, distinctUntilChanged, filter } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { fromEvent, interval, timer } from 'rxjs';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { PackageDialogComponent } from 'src/app/shared/dialogs/package-dialog/package-dialog.component';
import { ComboDialogComponent } from 'src/app/shared/dialogs/combo-dialog/combo-dialog.component';
import { AuthService } from 'src/app/services/auth.service';
import { ComboPopupsComponent } from '../combo-popups/combo-popups.component';
import { PackagepopupComponent } from '../packagepopup/packagepopup.component';
import { OrderService } from 'src/app/services/order.service';
import { BreakfastPopupComponent } from '../breakfast-popup/breakfast-popup.component';
import { ItemLevalPackageComponent } from '../item-leval-package/item-leval-package.component';

declare var $: any;

@UntilDestroy()
@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss'],
})
export class RecipeComponent
  implements OnInit, DoCheck, AfterViewInit, OnDestroy {
  @ViewChild('searchInput') searchInput: ElementRef;
  public reviewOrdersCount;
  public newMenu = [];
  public category = [];
  public subCategory = [];
  public loading = false;

  public subSectionMenu = [];
  public sectionCategory = [];



  constructor(
    private OrderService: OrderService,
    private cartService: CartService,
    private spinner: NgxSpinnerService,
    private modalService: BsModalService,
    public autthservice: AuthService
  ) {

  }

  ngOnInit(): void {
    this.calcReviewCount();
    this.getItems();

  }


  ngAfterViewInit() {
    fromEvent(this.searchInput.nativeElement, 'keyup')
      .pipe(
        untilDestroyed(this),
        map((evt: any) => evt.target.value),
        debounceTime(100),
        distinctUntilChanged()
      )
      .subscribe((text: string) => {
        if (!text) {
          console.log('!text', text)
          this.newMenu = this.dummymenu;
        }
        else {
          let filteredItems = [];
          filteredItems = this.dummymenu.filter(
            (item) =>
              item.itemName.toLowerCase().indexOf(text.toLowerCase()) > -1 || item.description?.toLowerCase().indexOf(text.toLowerCase()) > -1
          );
          this.newMenu = filteredItems;
          // this.dummymenu = filteredItems;
          if (this.cartService.cart.menuItems.length) {
            this.assignQuantity();
          }
          // this.filterarray();


        }
        let cat = new Map();
        let sub = new Map();
        for (let obj of this.newMenu) {
          cat.set(obj.sectionName, obj);
          sub.set(obj.subSectionName, obj);
        }
        this.category = [...cat.values()];
        this.subCategory = [...sub.values()];
      });
  }

  /**
   * function : getItems
   * purpose  : getting menuItems to display and if already cart contains
   * any items assign those items quanity to the menulist
   */

  public getItems(): void {
    this.spinner.show();
    this.loading = true;
    this.cartService
      .getItems()
      .pipe(untilDestroyed(this))
      .subscribe(
        (res) => {
          this.newMenu = res.itemsList;
          console.log(this.newMenu)
          this.newMenu.map(x => { x.actualPrice = x.price })
          this.dummymenu = res.itemsList;
          if (this.cartService.cart.menuItems.length) {
            this.assignQuantity();
          }
          if (this.cartService.cart.package.length) {
            this.assignPackageQty();
          }
          if(this.cartService.cart.combo.length){
            this.assignComboQty();
          }

          let temp = [];
          res.itemsList.forEach(element => {
            temp.push(element.sectionName);
          });
          this.sectionCategory = [...new Set(temp)];
          this.filterarray();
          // this.loadSubsection(this.subCategory[0])
        },
        (err) => {
          this.loading = false;
          this.spinner.hide();
        },
        () => {
          this.loading = false;
          this.spinner.hide();
        }
      );
  }
  /**
   * function : assignQuantity
   * purpose  : if already cart contains
   * any items assign those items quanity to the menulist
   */
  public assignQuantity(): void {
    if (this.cartService.cart.menuItems.length) {
      this.cartService.cart.menuItems.forEach((cartItem) => {
        this.newMenu.forEach((menuItem) => {
          if (cartItem.itemId === menuItem.itemId) {
            menuItem.quantity = +cartItem.quantity;
            menuItem.addonSelected = cartItem?.addonSelected;
            menuItem.price = cartItem.price;
          }
        });
      });
    }
  }

  ngDoCheck() {
    this.calcReviewCount();
    this.assignPackageQty();
    this.assignComboQty();
  }

  /**
   * function : assignPackageQty
   * purpose  : when package modal open and close updating the quantity in menuitems
   */

  public assignPackageQty() {
    this.cartService.cart.package.forEach((cartItem) => {
      this.newMenu.forEach((menuItem) => {
        if (cartItem.PackageDetails.EcateringItemId === menuItem.itemId) {
          menuItem.quantity = +cartItem.PackageDetails.PkgQty;
          menuItem.price=+cartItem.PackageDetails.packageCost

        }
      });
    });
  }

  /**
   * function : assignComboQty
   * purpose  : when combo modal open and close updating the quantity in menuitems
   */

  public assignComboQty() {
    this.cartService.cart.combo.forEach((cartItem) => {
      this.newMenu.forEach((menuItem) => {
        if (cartItem.ComboDetails.EcateringItemId === menuItem.itemId) {
          menuItem.quantity = +cartItem.ComboDetails.comboQty;
        }
      });
    });
  }

  /**
   * function : addToCart
   * purpose  : adding the item to Cart (if already cart contain same item it will increase quantity)
   * @param product
   */

  public addToCart(product) {
    // combo
    if (product.packageComboFlag === 2) {
      this.modalService.show(ComboDialogComponent, {
        class: 'modal-dialog-custom modal-lg modal-dialog-centered',
        initialState: { product },
        keyboard: false,
      });
    }

    //  package

    if (product.packageComboFlag === 1) {
      this.modalService.show(PackageDialogComponent, {
        class: 'modal-dialog-custom modal-lg modal-dialog-centered',
        initialState: { product },
        keyboard: false,
      });
    }

    // menuitem
    if (product.packageComboFlag === 0) {
      product.quantity++;
      console.log(product)
      const productExistInCart = this.cartService.cart.menuItems.find(
        ({ itemId }) => itemId === product.itemId
      );
      if (!productExistInCart) {
        this.cartService.cart.menuItems.push({ ...product });
        return;
      }
      this.cartService.increaseItemQuantity(product.itemId);
      this.calcReviewCount();
    }
  }
  public newmodel(product) {
    console.log(product)

    // combo
    if (product.packageComboFlag === 2) {
      this.modalService.show(ComboPopupsComponent, {
        // this.modalService.show(ComboPopupsComponent, {
        class: 'modal-combo modal-scrollable modal-centered',
        initialState: { product },
        keyboard: false,
      });
    }



    //  package

    if (product.packageComboFlag === 1) {

      if (product.packageItemLevelPriceFlag == 1) {
        this.modalService.show(ItemLevalPackageComponent, {
          class: 'modal-combo modal-scrollable modal-centered',
          initialState: { product },
          keyboard: false,
        });

      } else {
        this.modalService.show(PackagepopupComponent, {
          class: 'modal-combo modal-scrollable modal-centered',
          initialState: { product },
          keyboard: false,
        });
      }




    }

    // menuitem
    if (product.packageComboFlag === 0) {
      product.quantity++;
      const productExistInCart = this.cartService.cart.menuItems.find(
        ({ itemId }) => itemId === product.itemId
      );
      if (!productExistInCart) {
        this.cartService.cart.menuItems.push({ ...product });
        return;
      }
      this.cartService.increaseItemQuantity(product.itemId);
      // this.calcReviewCount();
    }
    // new code added 
    // if(product === 3){
    //   this.modalService.show(BreakfastPopupComponent, {
    //     class: 'modal-combo modal-scrollable modal-centered',
    //     initialState: { product },
    //     keyboard: false,
    //   });
    // }
  }

  /**
   * function : decreaseQuantity
   * purpose  : removing the item from Cart (if already cart contain same item it will decrease quantity)
   * @param product
   */

  public decreaseQuantity(product) {

    // decreasing if product is combo
    if (product.packageComboFlag === 2) {
      if (product.quantity === 1) {
        this.cartService.cart.combo.forEach((cartItem, i) => {
          if (cartItem.ComboDetails.EcateringItemId === product.itemId) {
            this.cartService.cart.combo.splice(i, 1);
          }
        });
      } else {
        this.modalService.show(ComboPopupsComponent, {
          // this.modalService.show(ComboPopupsComponent, {
          class: 'modal-combo modal-scrollable modal-centered',
          initialState: { product },
          keyboard: false,
        });
      }
    }

    // decreasing if product is package
    if (product.packageComboFlag === 1) {
      if (product.quantity === 1) {
        this.cartService.cart.package.forEach((cartItem, i) => {
          if (cartItem.PackageDetails.EcateringItemId === product.itemId) {
            this.cartService.cart.package.splice(i, 1);
          }
          if(product.packageItemLevelPriceFlag == 1)product.price=0;
        });
      } else {
        if (product.packageItemLevelPriceFlag == 1) {
          this.modalService.show(ItemLevalPackageComponent, {
            class: 'modal-combo modal-scrollable modal-centered',
            initialState: { product },
            keyboard: false,
          });

        } else {
          this.modalService.show(PackagepopupComponent, {
            class: 'modal-combo modal-scrollable modal-centered',
            initialState: { product },
            keyboard: false,
          });
        }
      }
    }

    product.quantity--;
    if (product.quantity === 0) {
      this.cartService.cart.menuItems.forEach((x, i) => {
        if (x.itemId === product.itemId) {
          this.cartService.cart.menuItems.splice(i, 1);
        }
      });
    } else {
      this.cartService.cart.menuItems.forEach((x, i) => {
        if (x.itemId === product.itemId) {
          this.cartService.cart.menuItems[i].quantity--;
        }
      });
    }
  }

  /**
   * function : calcReviewCount
   * purpose  : calculating the total quantity
   * @param product
   */

  public calcReviewCount() {
    let count = [];
    this.cartService.cart.menuItems.forEach((x) => {
      count.push(+x.quantity);
    });
    this.cartService.cart.package.forEach((x) => {
      count.push(+x.PackageDetails.PkgQty);
    });
    this.cartService.cart.combo.forEach((x) => {
      count.push(+x.ComboDetails.comboQty);
    });
    this.reviewOrdersCount = count.reduce((a, b) => a + b, 0);
  }

  ngOnDestroy(): void { }

  // toggle
  icontoggle() {
    $('#sidebar-wrapper').toggleClass('hide');
  }

  /**
 * /getItemAvailability method used to display availbility popup 
 
 */
  public itemAvailability: Array<any> = [];
  public alldaysAvailbilty: boolean = false;
  public itemavailbledays = [];
  public itempopupname: string = '';
  public getItemAvailability(item) {
    this.itemavailbledays = [];
    this.alldaysAvailbilty = false;
    this.itempopupname = item.itemName;
    try {
      this.spinner.show();
      this.OrderService.getItemAvailability(item.itemId).subscribe(res => {
        console.log(res);
        let data: any = res;
        this.itemAvailability = data.itemAvailability;
        console.log(this.itemAvailability)
        this.itemAvailability.map(x => {
          if (x.sunday == 1 && x.monday == 1 && x.tuesday == 1 && x.wednesday == 1 && x.thursday == 1 && x.friday == 1 && x.saturday == 1) {
            this.alldaysAvailbilty = true;
          } else {
            x.sunday == 1 ? this.itemavailbledays.push('Sunday') : '';
            x.monday == 1 ? this.itemavailbledays.push('Monday') : '';
            x.tuesday == 1 ? this.itemavailbledays.push('Tuesday') : '';
            x.wednesday == 1 ? this.itemavailbledays.push('Wednesday') : '';
            x.thursday == 1 ? this.itemavailbledays.push('Thursday') : '';
            x.friday == 1 ? this.itemavailbledays.push('Friday') : '';
            x.saturday == 1 ? this.itemavailbledays.push('Saturday') : '';

          }
          console.log(this.alldaysAvailbilty)
          $('#itemavail').modal({ backdrop: true }, 'show')


        })

      }, err => {
        this.spinner.hide();

      }, () => { this.spinner.hide(); })
    } catch (error) {

    }
  }

  public glutenFree: boolean = false;
  public nutFree: boolean = false;
  public spicy: boolean = false;
  public vegan: boolean = false;
  public vegetarian: boolean = false;
  public dummymenu = [];
  public apply: boolean = false;
  public domglutenFree: boolean = false;
  public domnutFree: boolean = false;
  public domspicy: boolean = false;
  public domvegan: boolean = false;
  public domvegetarian: boolean = false;

  public openFilter() {
    this.apply = true;
  }
  public filterdata(flag, event) {
    if (flag == "nutFree") {
      this.domnutFree = event.target.checked;
    } else if (flag == "glutenFree") {
      this.domglutenFree = event.target.checked;
    } else if (flag == "spicy") {
      this.domspicy = event.target.checked;

    } else if (flag == 'vegan') {
      this.domvegan = event.target.checked;

    } else if (flag == 'vegetarian') {
      this.domvegetarian = event.target.checked;
    }
    // this.filterarray();
  }
  public filterarray() {
    this.glutenFree = this.domglutenFree;
    this.nutFree = this.domnutFree;
    this.spicy = this.domspicy;
    this.vegan = this.domvegan;
    this.vegetarian = this.domvegetarian;
    this.apply = false;

    this.spinner.show();
    // console.log(this.dummymenu)
    if (!this.nutFree && !this.glutenFree && !this.spicy && !this.vegan && !this.vegetarian) {
      this.newMenu = this.dummymenu;
    } else {
      this.newMenu = this.dummymenu;
      if (this.glutenFree) {
        this.newMenu = this.newMenu.filter(x => {
          return x.glutenFree == 1;
        })
      }
      if (this.nutFree) {
        this.newMenu = this.newMenu.filter(x => {
          return x.nutFree == 1;
        })
      }
      if (this.vegan) {
        this.newMenu = this.newMenu.filter(x => {
          return x.vegan == 1;
        })
      }
      if (this.vegetarian) {
        this.newMenu = this.newMenu.filter(x => {
          return x.vegetarian == 1;
        })
      }
      if (this.spicy) {
        this.newMenu = this.newMenu.filter(x => {
          return x.spicy == 1;
        })
      }




    }
    let cat = new Map();
    let sub = new Map();
    for (let obj of this.newMenu) {
      cat.set(obj.sectionName, obj);
      sub.set(obj.subSectionName, obj);
    }
    this.category = [...cat.values()];
    this.subCategory = [...sub.values()];
    // console.log(this.newMenu)
    this.spinner.hide();
  }
  public filterClose() {
    this.apply = false;
    this.domglutenFree = this.glutenFree;
    this.domnutFree = this.nutFree;
    this.domspicy = this.spicy;
    this.domvegan = this.vegan;
    this.domvegetarian = this.vegetarian;
  }


  public onAddOn(item, index) {
    let setPrice;
    console.log(item, index)
    this.newMenu.map(x => {
      if (x.itemId == item.itemId) {
        console.log(item.itemName);
        if (item.addonSelected == "undefined") {
          this.newMenu[index].price = item.actualPrice;
          setPrice = item.actualPrice;
        }
        else {
          console.log(item.addonSelected)
          for (let i = 0; i < x.itemAddOnsList.length; i++) {
            if (x.itemAddOnsList[i].addOnId == item.addonSelected) {
              this.newMenu[index].price = x.itemAddOnsList[i].addOnPrice;
              setPrice = x.itemAddOnsList[i].addOnPrice;
            }
          }
        }
      }
    });
    console.log(this.cartService.cart.menuItems)
    this.cartService.cart.menuItems.forEach((x, i) => {
      if (x.itemId === item.itemId) {
        console.log(i);
        this.cartService.cart.menuItems[i] = item;
        this.cartService.cart.menuItems[i].price = setPrice;
      }
    });
  }
}
