import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PackagepopupComponent } from './packagepopup.component';

describe('PackagepopupComponent', () => {
  let component: PackagepopupComponent;
  let fixture: ComponentFixture<PackagepopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackagepopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackagepopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
