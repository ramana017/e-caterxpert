import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { CartService } from 'src/app/services/cart.service';
import { ValidationAlertDialogComponent } from 'src/app/shared/dialogs/validation-alert-dialog/validation-alert-dialog.component';
import Swal from 'sweetalert2';
declare var $: any;
const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 5000,
  timerProgressBar: true,
  didOpen: (toast) => {
    
    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  }
})
@Component({
  selector: 'app-packagepopup',
  templateUrl: './packagepopup.component.html',
  styleUrls: ['./packagepopup.component.scss']
})
export class PackagepopupComponent implements OnInit {
  public photo: string = 'assets/images/cater/01.png';
  public packageData;
  public packageData2; // is for original pkgitemqty when packagetype =0 multiply with total quantity
  public packQuantity;
  public packItemQuantity = [];
  public itemsTotalQuantity;
  public showSummary = false;
  public product; // coming from component reciepe component as intial value
  constructor(private spinner: NgxSpinnerService,
    private cartService: CartService, private modalRef: BsModalRef, private modalService: BsModalService) { }

  ngOnInit(): void {
    this.getComboItems(this.product.itemId);
  }

  public getComboItems(itemId): void {
    this.spinner.show();
    this.cartService.getPackage(itemId)
      .subscribe((data) => {
        console.log(data)

        this.packageData = data;
        this.packageData2 = JSON.parse(JSON.stringify(data));
        this.packQuantity = this.packageData?.PackageDetails?.pkgMinOrderQty;

        this.packageData.PackageItems.map(x => {
          x.checked = false;
        })
        this.assignQuantity();
        this.multiplyItems();
      },
      err => { this.spinner.hide(); 
        Toast.fire({
          icon: 'error',
          title: 'Failed to load Items!'
        })
     }, () => { this.spinner.hide(); });
  }

  /**
   * methhod is used to increase or decrease the quantity of each individual item
   */
  public changeItemQuantity(index, flag) {
    flag == 'plus' ? this.packageData.PackageItems[index].pkgItemQty = this.packageData.PackageItems[index].pkgItemQty + 1 : this.packageData.PackageItems[index].pkgItemQty = this.packageData.PackageItems[index].pkgItemQty - 1
  }
  public counter = 0;
  public itemChecked(event, index) {
    var limit = this.packageData?.PackageDetails?.pkgMaxItems;
    //  console.log(event.target.checked);
    if (event.target.checked === true) {
      this.counter++;
      this.packageData.PackageItems[index].checked = true;

    } else {
      this.packageData.PackageItems[index].checked = false;
      this.packageData.PackageItems[index].pkgItemQty = 0;
      this.counter--;
    }


  }

  changeTotalQty(flag) {
    this.packQuantity = flag == "plus" ? this.packQuantity + 1 : this.packQuantity - 1;
    this.multiplyItems();
  }

  multiplyItems() {
    if (this.packageData.PackageDetails.packageType === '0' && this.packQuantity > 0) {
      this.packageData2.PackageItems.forEach((x) => {
        this.packageData.PackageItems.forEach(element => {
          if (x.pkgItemId === element.pkgItemId) {
            element['pkgItemQty'] = x.pkgItemQty * this.packQuantity;
          }
        });
      });
    }
  }

  closeModal() {
    this.modalRef.hide();
  }

  /**
   * function : assignQuantity
   * purpose : if the selected item already in the cart we are assigning the quantity on load
   */
  assignQuantity() {
    if (this.cartService.cart.package.length) {
      this.cartService.cart.package.find(
        ({ PackageDetails, PackageItems }) => {
          if (PackageDetails.packageId === this.packageData.PackageDetails.packageId) {
            this.packageData['PackageDetails'] = JSON.parse(JSON.stringify(PackageDetails));
            this.packageData['PackageItems'] = JSON.parse(JSON.stringify(PackageItems));
            this.packQuantity = this.packageData.PackageDetails.PkgQty;
          this.packQuantity=(+this.packageData.PackageDetails.PkgQty);
            // this.packQuantity = +$(`#pkgqty`).val();
            // pkgqty
            this.packageData.PackageItems.forEach((x, i) => {
             x.pkgItemQty=x.pkgItemQty;
             x.checked==true?this.counter++:'';
            });
          }
        }
      );
      this.cartService.cart.package
    }
    
  }
public dummyArray=[];
  onSave() {
    this.dummyArray=[];
    this.packageData.PackageDetails.PkgQty=this.packQuantity;
    console.log("In save method");
    let total = []
    this.packageData.PackageItems.map(x => {
      total.push(x.pkgItemQty)
      if(x.pkgItemQty>0){
        this.dummyArray.push(x);
      }
    })
    this.itemsTotalQuantity = total.reduce((a, b) => a + b, 0);


    if (this.packageData.PackageDetails.packageType === '1' &&
      this.packageData.PackageDetails.pkgMaxItems !== '0' &&
      this.packageData.PackageDetails.pkgMinItems !== '0') {
      if (this.packageData.PackageItems.filter(x => x.pkgItemQty > 0).length >= this.packageData.PackageDetails.pkgMinItems
        && this.packageData.PackageItems.filter(x => x.pkgItemQty > 0).length <= this.packageData.PackageDetails.pkgMaxItems
        && this.itemsTotalQuantity == this.packQuantity * this.packageData.PackageDetails.pkgMaxSelectItemQty) {
        // this.takeOrder();
        this.showSummary = true;
      } else {
        if ((this.packageData.PackageItems.filter(x => x.pkgItemQty > 0).length < this.packageData.PackageDetails.pkgMinItems
        || this.packageData.PackageItems.filter(x => x.pkgItemQty > 0).length > this.packageData.PackageDetails.pkgMaxItems)
        && this.itemsTotalQuantity != this.packQuantity * this.packageData.PackageDetails.pkgMaxSelectItemQty) {
          console.log('160')
          this.modalService.show(ValidationAlertDialogComponent, {
            class: 'modal-dialog-custom ',
            initialState: { message: `Please select between ${this.packageData?.PackageDetails?.pkgMinItems} and ${this.packageData?.PackageDetails?.pkgMaxItems} items from this list. The total item quantity should be equal to  ${this.packQuantity * this.packageData.PackageDetails.pkgMaxSelectItemQty}.` },
            keyboard: false,
          });
          // alert(`Please select between ${this.packageData?.PackageDetails?.pkgMinItems} and ${this.packageData?.PackageDetails?.pkgMaxItems} items from this list. The total item quantity should be equal to  ${this.packQuantity}.`);
        } else if(this.itemsTotalQuantity != this.packQuantity * this.packageData.PackageDetails.pkgMaxSelectItemQty) {
          console.log('168')
          this.modalService.show(ValidationAlertDialogComponent, {
            class: 'modal-dialog-custom ',
            initialState: { message: `The total item quantity should be  equal to  ${this.packQuantity * this.packageData.PackageDetails.pkgMaxSelectItemQty}.` },
            keyboard: false,
          });
          // alert(`Please select ${this.packageData?.PackageDetails?.pkgMinItems} item(s) from this list. The total item quantity should be  equal to  ${this.packQuantity}.`);
        }else if(this.packageData.PackageItems.filter(x => x.pkgItemQty > 0).length < this.packageData.PackageDetails.pkgMinItems
        || this.packageData.PackageItems.filter(x => x.pkgItemQty > 0).length > this.packageData.PackageDetails.pkgMaxItems){
          this.modalService.show(ValidationAlertDialogComponent, {
            class: 'modal-dialog-custom ',
            initialState: { message: `Please select between ${this.packageData?.PackageDetails?.pkgMinItems} and ${this.packageData?.PackageDetails?.pkgMaxItems} items from this list.` },
            keyboard: false,
          });
        }
      }
    }


    if (this.packageData.PackageDetails.packageType === '1' &&
      this.packageData.PackageDetails.pkgMaxItems === '0' &&
      this.packageData.PackageDetails.pkgMinItems === '0') {
      if (this.itemsTotalQuantity == this.packQuantity * this.packageData.PackageDetails.pkgMaxSelectItemQty) {
        //  this.takeOrder();
        this.showSummary = true;
      } else {
        this.modalService.show(ValidationAlertDialogComponent, {
          class: 'modal-dialog-custom ',
          initialState: { message: `Total items quantity should  equal to ${this.packageData.PackageDetails.PkgQty * this.packageData.PackageDetails.pkgMaxSelectItemQty}` },
          keyboard: false,
        });
        //  alert(`Total items quantity should  equal to ${this.packageData.PackageDetails.PkgQty}`);
      }
    }


    if (this.packageData.PackageDetails.packageType === '0') {
      //  this.takeOrder();
      this.showSummary = true;
    }

  }





  takeOrder() {
    console.log("tyyey")
    this.cartService.cart.package.forEach((x, i) => {
      if (this.packageData.PackageDetails.packageId === x.PackageDetails.packageId) {
        this.cartService.cart.package[i] = this.packageData;
      }
    });
    const productExistInCart = this.cartService.cart.package.find(
      ({ PackageDetails }) => PackageDetails.packageId === this.packageData.PackageDetails.packageId);

    if (!productExistInCart) {
      this.cartService.cart.package.push(this.packageData);
    }
    console.log(this.cartService.cart)
    this.modalRef.hide();
  }




}
