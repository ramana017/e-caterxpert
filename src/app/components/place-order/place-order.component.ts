import { Component, OnInit, OnDestroy } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { OrderService } from 'src/app/services/order.service';
import { DatePipe } from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-place-order',
  templateUrl: './place-order.component.html',
  styleUrls: ['./place-order.component.scss']
})
export class PlaceOrderComponent implements OnInit, OnDestroy {
  public cartnum=1;
  public cart;
  public orderDetails;
  public loginres;
  public customerAddress;
  public totalAmount;
  public taxAmount = 0;
  public customerData: boolean = false;
  public sysDate = this.datepipe.transform(new Date(), 'MM/dd/yyyy');
  public discountpercent;
  public discountFlatAmount;

  constructor(public cartService: CartService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private orderService: OrderService,
    public authservice: AuthService,
    private datepipe: DatePipe) {
    this.orderDetails = JSON.parse(sessionStorage.getItem('orderDetails'));
    this.loginres = JSON.parse(sessionStorage.getItem('loginResponse'));
    this.catererId = (sessionStorage.getItem('catererId'));
    this.backUrl = localStorage.getItem('webServiceUrl') + '/updateOrderPaymentStatus?application=eCaterXpert';
    this.discountpercent=this.orderDetails.discountPercentage;
    this.discountFlatAmount = this.orderDetails.discountFlat;

  }

  ngOnInit(): void {

    this.getDataForPaymentDetails();
    this.cart = this.cartService.cart;
    this.getCustomerAdress();
    console.log(this.orderDetails)
    this.totalAmount = this.cartService.cartTotalAmount();
    this.getTax();
  }

  /**
   * function : getCustomerAdress
   * purpose  : getting the customer address to display left side of the place order screen
   */

  public getCustomerAdress() {
    this.orderService.getCustomerAddress(this.loginres.AuthenticateUser.customerId)
      .subscribe((res) => {
        console.log("ress", Object.keys(res).length)
        this.customerAddress = res.CustBillAddressObject;
        if (this.customerAddress) {
          Object.keys(this.customerAddress).length > 0 ? this.customerData = true : this.customerData = false

        }
        console.log(this.customerAddress)

      });
  }

  /**
   * function : getCustomerAdress
   * purpose  : getting the customer address to display left side of the place order screen
   */

  public getTax() {
    this.orderService.getTaxAmount(this.orderDetails.orderNumber)
      .subscribe((data) => {
        let tax = data;
        this.taxAmount = tax.countytaxamount + tax.statetaxamount + tax.othertaxamount;
      });
  }

  /**
   * function : confirmOrder
   * purpose  : confirm order and navigating to the order-confirmation page
   */

  public confirmOrder() {
    this.spinner.show();
    this.orderService.confirmOrder(this.orderDetails.orderNumber).subscribe((res) => {
      let totalamount: number = (this.totalAmount+this.taxAmount-(this.totalAmount)*(this.discountpercent/100))
      sessionStorage.setItem('totalvalues', totalamount.toFixed(2))
      this.router.navigate(['order-confirmation']);
    }, err => { this.spinner.hide(); }, () => { this.spinner.hide(); });
  }

  ngOnDestroy(): void {
    // sessionStorage.removeItem('orderDetails');
  }

  // paymentroute() {
  //   console.log("ddd")
  //   this.orderDetails.totalamount = (this.taxAmount + this.totalAmount)
  //   sessionStorage.setItem('orderDetails', JSON.stringify(this.orderDetails))
  //   this.router.navigate(['order-confirmation']);

  // }
  public catererId;
  public paymentGatewayURL;
  public backUrl;
  public paymentId;

  public getDataForPaymentDetails() {
    this.spinner.show()
    try {
      this.spinner.hide()
      this.orderService.getDataForPaymentDetails().subscribe(res => {
        console.log(res)
        let data: any = res;
        // this.creditCardTypes = data.creditCardTypes;
        this.paymentGatewayURL = data.paymentGatewayURL;
      }, err => { this.spinner.hide(); }, () => {
        this.spinner.hide();
      })
    } catch (error) {

    }
  }

  /**
   * name
   */
  public saveOrderPayment() {

    let param =
    {
      "OrderPaymentDetails":
      {

        "orderNumber": this.orderDetails.orderNumber,
        "ccTypeId": '',
        "ccFeePerct": '',
        "ccPayAmt": '',
        "ccFeeAmt": '',
        "totAmt": (this.totalAmount+this.taxAmount-(this.totalAmount)*(this.discountpercent/100)),
        "comments": '',
        "updatedUser": this.loginres.AuthenticateUser.userId,
        "paymentTypeId": 1,
        "custEmail": this.orderDetails.email
      }
    }

    try {
      this.orderService.saveOrderPayment(param).subscribe(res => {
        console.log(res)
        let data: any = res;
        this.paymentId = data.orderPaymentId;
        let totalamount: number = (this.totalAmount+this.taxAmount-(this.totalAmount)*(this.discountpercent/100))
        sessionStorage.setItem('totalvalues', totalamount.toFixed(2))
  
        sessionStorage.setItem('orderDetails', JSON.stringify(this.orderDetails))

        setTimeout(() => {
          document.forms[0].action = this.paymentGatewayURL;
          document.forms[0].submit();
        }, 1000)


      })
    } catch (error) {

    }
  }


}
