import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CombodailougeorderComponent } from './combodailougeorder.component';

describe('CombodailougeorderComponent', () => {
  let component: CombodailougeorderComponent;
  let fixture: ComponentFixture<CombodailougeorderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CombodailougeorderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CombodailougeorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
