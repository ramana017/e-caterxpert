import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { CartService } from 'src/app/services/cart.service';
import { ValidationAlertDialogComponent } from 'src/app/shared/dialogs/validation-alert-dialog/validation-alert-dialog.component';
import Swal from 'sweetalert2';
declare var $;
const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  }
})
@Component({
  selector: 'app-combodailougeorder',
  templateUrl: './combodailougeorder.component.html',
  styleUrls: ['./combodailougeorder.component.scss']
})
export class CombodailougeorderComponent implements OnInit {

  public dummycombodetails: any = {};
  public dummycomboitems: Array<any> = [];
  public comboData: any;
  public showSummary: boolean = false;
  public comboQuantityTotal: number;
  public product; // coming from component reciepe component as intial value
  constructor(private spinner: NgxSpinnerService,
    private cartService: CartService, private modalRef: BsModalRef,
    private modalService: BsModalService) { }

  ngOnInit(): void {
    this.getComboItems(this.product.itemId);
  }
  /**
   * toogle for menu
   */
  public toogle(type, j) {
    $(function () {
      $(`#${type + j}`).next().slideToggle();
      $(`#${type + j}`).children('i').toggleClass('transform');
    });
  }

  public getComboItems(itemId): void {
    this.spinner.show();
    this.cartService.getCombo(itemId)
      .subscribe((data) => {
        console.log(data);
        this.comboData = data;
        this.comboData.ComboDetails.comboQty = 1;
        this.comboQuantityTotal = this.comboData.ComboDetails.comboQty;;
        this.dummycomboitems = this.comboData.ComboItems;
        this.dummycombodetails = this.comboData.ComboDetails;
        let i = 0;
        this.dummycomboitems.map(x => {
          x.id = i;
          x.itemQty = +(x.itemQty)
          i++;
        })

      }, err => {
        this.spinner.hide();
        Toast.fire({
          icon: 'error',
          title: 'Failed to load Items!'
        })
      }, () => { this.spinner.hide(); });

  }
  public itemQuantitychange(flag, id) {
    this.dummycomboitems.map(x => {
      if (x.id === id) {
        x.itemQty = flag == 'minus' ? +(x.itemQty) - 1 : +(x.itemQty) + 1;
      }
    })
  }
  public totalQuantityChange(flag) {
    console.log(flag)
    flag == 'minus' ? this.dummycombodetails.comboQty = this.dummycombodetails.comboQty - 1 : this.dummycombodetails.comboQty = this.dummycombodetails.comboQty + 1;
    this.comboQuantityTotal = this.dummycombodetails.comboQty;
  }
  closeModal() {
    this.modalRef.hide();
  }


  public takeOrder() {

  }
 public packeges=[];
  public onSave() {
    this.packeges=[];
    this.dummycomboitems.map(x => {
      if (x.packageId!=="0") {
        let currentpackege=[];
        this.dummycomboitems.map(y => {
            if(x.packageId===y.packageId&& !this.packeges.includes(y.packageId)){
                  currentpackege.push(y)
            }

        })
        let obj={packagedetails:x,packages:currentpackege}
        this.packeges.push(obj)

      }

    })
console.log(this.packeges)
  }

  public checking(x):boolean{
    if(this.packeges.length==0){
      return false;
    }else{
        this.packeges
    }
  }
  
}
