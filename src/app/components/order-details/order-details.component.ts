import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { CartService } from 'src/app/services/cart.service';
import { dateTransform, OrderService } from 'src/app/services/order.service';
import { NgxSpinnerService } from 'ngx-spinner';

declare var $: any;
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {
  public orderDetails: FormGroup;
  public stateList = [];
  public addressList = [];
  keyword = 'street';
  public systemdate: Date = new Date();
  public loginres: any;

  constructor(private fb: FormBuilder,
    private router: Router,
    private date: DatePipe,
    public authService: AuthService,
    public cartService: CartService,
    private orderService: OrderService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit(): void {

    this.orderDetailsForm();


    // $('input').on('focusin', function() {
    //   $(this).parent().find('label').addClass('active');
    // });

    // $('input').on('focusout', function() {
    //   if (!this.value) {
    //     $(this).parent().find('label').removeClass('active');
    //   }
    // });

  }
  ngAfterViewInit() {
    this.systemdate = new Date();
    this.getStateList();
    this.getAddressList();
  }

  orderDetailsForm(): void {
    this.orderDetails = this.fb.group({
      orderNumber: ['', Validators.required],
      orderName: ['', Validators.required],
      orderType: ['Delivery', Validators.required],
      date: ['', Validators.required],
      orderEndDate: [''],
      eventStartDate: [''],
      eventEndDate: [''],
      name: ['', Validators.required],
      phone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      email: ['', [Validators.required, Validators.email]],
      street: ['', Validators.required],
      suite: [''],
      city: ['', Validators.required],
      state: ['', Validators.required],
      zipCode: ['', Validators.required],
      statename: [''],
      notes: [''],
      guestCount: [null],
      discountCode: [''],
      discountFlat: [''],
      discountPercentage: [0],
      billingCode: [''],
      address: ['']
    });
    this.loginres = JSON.parse(sessionStorage.getItem('loginResponse'));
    const repeatOrderres = JSON.parse(sessionStorage.getItem('orderDetails'));
    if (this.loginres) {
      this.orderDetails.get('orderNumber').setValue(this.loginres.AuthenticateUser.newOrderId);
    }
    if (repeatOrderres) {
      this.setRepeatOrderDetails();
    }
  }
  public setRepeatOrderDetails() {
    var fromback = this.activatedRoute.snapshot.params.isBack;
    console.log(fromback == 1)

    const data = JSON.parse(sessionStorage.getItem('orderDetails'))
    console.log(data)
    this.orderDetails.get('orderName').setValue(data.orderName);
    this.orderDetails.get('orderType').setValue(data.orderType);
    if (data.date) {
      this.orderDetails.get('date').setValue(fromback == 1 ? new Date(data.date) : '');
    } if (data.orderEndDate) {
      this.orderDetails.get('orderEndDate').setValue(fromback == 1 ? new Date(data.orderEndDate) : '');
    }
    if (data.eventStartDate) {
      this.orderDetails.get('eventStartDate').setValue(fromback == 1 ? new Date(data.eventStartDate) : '');
    }
    if (data.eventEndDate) {
      this.orderDetails.get('eventEndDate').setValue(fromback == 1 ? new Date(data.eventEndDate) : '');
    }
    this.orderDetails.get('name').setValue(data.name);
    this.orderDetails.get('phone').setValue(data.phone);
    this.orderDetails.get('email').setValue(data.email);
    this.orderDetails.get('street').setValue(data.street);
    this.orderDetails.get('address').setValue(data.street);
    this.orderDetails.get('suite').setValue(data.suite ? data.suite : '');
    this.orderDetails.get('city').setValue(data.city);
    this.orderDetails.get('state').setValue(data.state);
    this.orderDetails.get('zipCode').setValue(data.zipCode);
    this.orderDetails.get('notes').setValue(data.notes ? data.notes : '');
    this.orderDetails.get('guestCount').setValue(data.guestCount ? data.guestCount : null);
    this.orderDetails.get('discountCode').setValue(data.discountCode ? data.discountCode : '');
    this.orderDetails.get('billingCode').setValue(data.billingCode ? data.billingCode : '');



    if (data.discountCode) {
      this.validateDiscountCode();
    }
    console.log(Date.parse(data.date) < Date.parse(dateTransform(new Date())));

    if (Date.parse(this.orderDetails.value.date) < Date.parse(dateTransform(new Date()))) {
      this.orderDetails.get('date').setValue(null);
      this.orderDetails.get('orderEndDate').setValue(null);
      this.orderDetails.get('eventStartDate').setValue(null);
      this.orderDetails.get('eventEndDate').setValue(null);
    }
  }
  public getDefaultDeliveryEndTime() {
    console.log("#########")
    let deliveryStartDate = Date.parse(this.orderDetails.controls['date'].value);
    this.orderService.getDefaultDeliveryEndTime().subscribe(res => {
      console.log(res);
      this.orderDetails.get('orderEndDate').setValue(new Date(deliveryStartDate + (res.defaultDeliveryEndTime * 60 * 1000)));
      this.orderDetails.get('eventStartDate').setValue(new Date(deliveryStartDate));
      this.orderDetails.get('eventEndDate').setValue(new Date(deliveryStartDate + (res.defaultDeliveryEndTime * 60 * 1000)));
    }, err => {

    })

  }
  public allowDigits(event) {
    console.log(event)
    // console.log(event);
    if (event.keyCode < 48 || event.keyCode > 57) {
      /*event.returnValue = false;*/
      event.preventDefault ? event.preventDefault() : event.returnValue = false;
    }
    return true;
  }
  public selectChange(e) {
    console.log(e);
    this.orderDetails.get('street').setValue(e.street);
    this.orderDetails.get('suite').setValue(e.suite);
    this.orderDetails.get('city').setValue(e.city);
    this.orderDetails.get('zipCode').setValue(e.zip);
    this.stateList.find((x) => {
      if (x.name === e.state) {
        this.orderDetails.get('state').setValue(x.id);
      }
    });
  }

  /**
   * function : getStateList
   * purpose : based on the user selected state assigning the id
   */

  public getStateList() {
    this.orderService.getStateList().subscribe((res) => {
      this.stateList = res.StateList;
    });
  }

  /**
   * function : getAddressList
   * purpose : list of address for autofilling the address fields
   */
  public getAddressList() {
    this.orderService.getAddressList().subscribe((res) => {
      this.addressList = res.LocationAddress;
    });
  }

  get orderDetailsData() {
    return this.orderDetails.controls;
  }


  public Submit(): void {
    console.log(this.orderDetails.value)
    if (this.orderDetails.value.guestCount == null) {
      console.log()
      this.orderDetails.get('guestCount').setValue(0);
    }
    this.spinner.show();
    this.orderDetails.get('date').setValue(this.date.transform(this.orderDetails.value.date, 'MM/dd/yyyy hh:mm a'));
    this.orderDetails.get('orderEndDate').setValue(dateTransform(this.orderDetails.value.orderEndDate));
    this.orderDetails.get('eventStartDate').setValue(dateTransform(this.orderDetails.value.eventStartDate));
    this.orderDetails.get('eventEndDate').setValue(dateTransform(this.orderDetails.value.eventEndDate));

    let menuItems = this.cartService.cart.menuItems.map(x => {
      return {
        "availabilityIndicator": x.availabilityIndicator,
        "replaceAttribute": x.replaceAttribute,
        "packageComboFlag": x.packageComboFlag,
        "nutFree": x.nutFree,
        "quantity": x.quantity,
        "subSectionSortorder": x.subSectionSortorder,
        "itemSortorder": x.itemSortorder,
        "sectionSortorder": x.sectionSortorder,
        "description": x.description,
        "glutenFree": x.glutenFree,
        "sectionId": x.sectionId,
        "vegan": x.vegan,
        "spicy": x.spicy,
        "subSectionId": x.subSectionId,
        "mandateAddons": x.mandateAddons,
        "sectionName": x.sectionName,
        "itemId": x.itemId,
        "itemName": x.itemName,
        "cxpServiceId": x.cxpServiceId,
        "price": x.price,
        "vegetarian": x.vegetarian,
        "replaceDesc": x.replaceDesc,
        "addonId": x.addonSelected ?? 0
      }
    })
    let formData = this.orderDetails.getRawValue();
    if (formData.discountPercentage == 0 && formData.discountFlat == 0) {
      formData.discountCode = '';
      formData.discountFlat = '';
    }


    let data = {
      OrderDetails: formData,
      menuItems: menuItems,
      package: this.cartService.cart.package,
      combo: this.cartService.cart.combo
    };


    let loginres = JSON.parse(sessionStorage.getItem('loginResponse'));

    if (loginres) {
      data.OrderDetails['customerId'] = loginres.AuthenticateUser.customerId;
      data.OrderDetails['regUserId'] = loginres.AuthenticateUser.userId;
    }
    this.stateList.find((x) => {
      if (x.id == this.orderDetails.value.state) {
        this.orderDetails.get('statename').setValue(x.name);
        console.log(x.name)
      }
    });



    sessionStorage.setItem('cartItems', JSON.stringify(this.cartService.cart))
    sessionStorage.setItem('orderDetails', JSON.stringify(this.orderDetails.getRawValue()));
    console.log(data);
    this.orderService.saveOrderDetails(data).subscribe((res) => {

      console.log(formData);
      let object = {
        "package": [],
        "OrderDetails": {
          "zip": formData.zipCode,
          "OrderName": formData.orderName,
          "CorpUserId": 0,
          "city": formData.city,
          "DeliveryOrPickupDatetime": formData.date,
          "OrderedDatetime": "",
          "CustomerId": 0,
          "OrderId": 0,
          "OrderType": formData.orderType,
          "suite": formData.suite,
          "HomePhone": formData.phone,
          "street": formData.street.street ? formData.street.street : formData.street,
          "GuestUserName": formData.name,
          "state": formData.state,
          "UserType": 0,
          "Notes": formData.notes,
          "email": formData.email,
          "guestCount": formData.guestCount,
          "billingCode": formData.billingCode,
          "discountCode": formData.discountCode



        },
        "menuItems": [],
        "combo": []
      }

      console.log(res);
      this.router.navigate(['place-order']);
    }, err => { this.spinner.hide(); }, () => { this.spinner.hide(); });
  }


  /**
   * validateDiscountCode method is usedto validate discount code
   */
  public discountresponse: any;
  public discountFlagPercent: boolean = false;
  public discountFlagFlat: boolean = false;
  public resultarray = [];
  public discountFlag = false;
  // public notavailableArray;
  public validateDiscountCode() {


    if (this.orderDetails.value.discountCode) {
      this.spinner.show();

      this.orderService.validateDiscountCode(this.orderDetails.value.discountCode, this.date.transform(this.orderDetails.value.date, 'MM/dd/yyyy'), this.loginres.AuthenticateUser.customerId).subscribe(
        res => {
          console.log(res)
          let data: any = res;
          this.discountFlagPercent = true;
          this.discountresponse = data;
          this.orderDetails.get('discountPercentage').setValue(data.discountPercentage);
          this.orderDetails.get('discountFlat').setValue(data.discountFlatAmount);
          if (data.discountPercentage > 0 || data.discountFlatAmount > 0) {
            this.discountFlag = true;
            this.orderDetails.controls['discountCode'].disable();
          }
          else {
            this.discountFlag = false;
            this.orderDetails.controls['discountCode'].enable();
          }
        }, err => { this.spinner.hide(); }, () => { this.spinner.hide(); }
      )
    } else {
      Swal.fire('Invalid', 'Discount Code cannot be empty', 'warning');
    }
  }
  public removeDiscountCode(event) {
    console.log(event)
    this.discountFlagPercent = false;
    this.orderDetails.controls['discountCode'].enable();
    this.discountFlag = false;
    this.orderDetails.get('discountCode').setValue('');
    this.orderDetails.get('discountPercentage').setValue(0);
    this.orderDetails.get('discountFlat').setValue(0);
  }

  public notAvailableArray = [];
  public validateItemAvailability() {
    console.log(this.orderDetails.value)

    if (this.orderDetails.invalid) {

    }

    let dStart = this.orderDetails.controls['date'].value;
    let dEnd = this.orderDetails.controls['orderEndDate'].value;
    let eStart = this.orderDetails.controls['eventStartDate'].value;
    let eEnd = this.orderDetails.controls['eventEndDate'].value;
    let err = '';
    if (dStart && dEnd) {

      if (Date.parse(dStart) > Date.parse(dEnd)) {
        err += 'Delivery Start Date cannot be greater than Delivery End Date <br> ';
      }

    }
    if (eStart && eEnd) {

      if (Date.parse(eStart) > Date.parse(eEnd)) {
        err += 'Event Start Date cannot be greater than Event End Date <br> ';
      }

    }
    if (err.length) {
      Swal.fire('Invalid', err, 'warning')
    } else {





      let items = [];
      this.notAvailableArray = []
      console.log(this.cartService.cart)
      this.cartService.cart.menuItems.map(x => {
        items.push(x.itemId);
      })
      this.cartService.cart.combo.map(x => {
        items.push(x.ComboDetails.EcateringItemId);
      })
      this.cartService.cart.package.map(x => {
        items.push(x.PackageDetails.EcateringItemId);
      })


      console.log('Before Filter', items)
      this.spinner.show();
      this.orderService.validateItemAvailability(items.toString(), this.date.transform(this.orderDetails.value.date, 'MM/dd/yyyy'), this.date.transform(this.orderDetails.value.date, 'hh:mm a')).subscribe(
        res => {
          console.log(res)
          let data: any = res;
          this.resultarray = []
          let localarray = data.itemIds ? (data.itemIds.split(',')) : []
          // let notavailableArray;
          // this.notavailableArray = items.filter(val => !this.resultarray.includes(val.toString()))
          localarray.map(x => {
            this.resultarray.push(+x)
          })
          console.log(this.resultarray, this.resultarray.length, localarray.length)
          if (this.resultarray.length == items.length) {
            this.Submit()
          } else {
            this.cartService.cart.combo.map(x => {
              let obj = { "name": x.ComboDetails.comboName, "qty": x.ComboDetails.comboQty }
              if (!this.resultarray.includes(x.ComboDetails.EcateringItemId)) {
                this.notAvailableArray.push(obj);
              }
            })
            this.cartService.cart.package.map(x => {
              let obj = { "name": x.PackageDetails.packageName, "qty": x.PackageDetails.PkgQty }
              if (!this.resultarray.includes(x.PackageDetails.EcateringItemId)) {
                this.notAvailableArray.push(obj);
              }
            })
            this.cartService.cart.menuItems.map(x => {
              let obj = { "name": x.itemName, "qty": x.quantity }
              if (!this.resultarray.includes(x.itemId)) {
                this.notAvailableArray.push(obj);
              }
            })

            $('#validateItemsModal').modal({ keyboard: false, backdrop: false }, 'show')
          }
          // console.log('afterfilter',notavailableArray)
        }, err => { this.spinner.hide(); }, () => { this.spinner.hide(); }
      )
    }
  }
  public deleteandproceed() {
    $('#validateItemsModal').modal('hide')
    this.cartService.cart.menuItems = this.cartService.cart.menuItems.filter(val => this.resultarray.includes(val.itemId));
    this.cartService.cart.package = this.cartService.cart.package.filter(val => this.resultarray.includes(val.PackageDetails.EcateringItemId));
    this.cartService.cart.combo = this.cartService.cart.combo.filter(val => this.resultarray.includes(val.ComboDetails.EcateringItemId));
    console.log("after remove", this.cartService.cart)
    if (this.cartService.cart.combo.length > 0 || this.cartService.cart.menuItems.length > 0 || this.cartService.cart.package.length > 0) {

      this.Submit();

    } else {
      Swal.fire({
        title: 'Cart is empty',
        text: ' Please add items and proceed',
        imageUrl: 'assets/images/cart.png',
        imageWidth: 100,
        imageHeight: 100,
        // icon: 'error',
        confirmButtonText: 'OK',
        allowOutsideClick: false
      }).then(ok => {
        console.log("yes")
        this.authService.redirectToHome();
      })
    }

  }

  onOrderTypeChange() {
    this.orderDetails.get('date').setValue(null);
    this.orderDetails.get('orderEndDate').setValue(null);
    this.orderDetails.get('eventStartDate').setValue(null);
    this.orderDetails.get('eventEndDate').setValue(null);
  }
}
