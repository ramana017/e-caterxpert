import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComboPopupsComponent } from './combo-popups.component';

describe('ComboPopupsComponent', () => {
  let component: ComboPopupsComponent;
  let fixture: ComponentFixture<ComboPopupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComboPopupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComboPopupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
