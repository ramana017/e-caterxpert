import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-payment-details',
  templateUrl: './payment-details.component.html',
  styleUrls: ['./payment-details.component.scss']
})

export class PaymentDetailsComponent implements OnInit {
  public creditCardTypes: Array<creditcardDetails> = [];
  public paymentForm: FormGroup;
  public formError: boolean = false;
  public paymentGatewayURL: string = '';

  public orderDetails: any;
  public loginResponse: any;
  public sysDate = this.datepipe.transform(new Date(), 'MM/dd/yyyy');
  public paymentId: number;
  public catererId: string;
  public backUrl: string;
  //   'http://localhost:5000/payment/'
  //  'https://catapps.aquilasoftware.com/SupervisorSignIn/resources/SupervisorSignIn/SupervisorSignIn.html#/'
  // http://localhost:4200/#/payment-response/'
  // 'https://catapps.aquilasoftware.com/SupervisorSignIn/resources/SupervisorSignIn/SupervisorSignIn.html#/'

  // http://localhost:4800/?catererId=ridgewellscat/#/

  constructor(private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private orderService: OrderService, public datepipe: DatePipe, public http: HttpClient) {
    this.orderDetails = JSON.parse(sessionStorage.getItem('orderDetails'));
    this.loginResponse = JSON.parse(sessionStorage.getItem('loginResponse'));
    this.catererId = (sessionStorage.getItem('catererId')).split('=')[1];
    this.backUrl = localStorage.getItem('webServiceUrl') + '/updateOrderPaymentStatus?catererId=' + this.catererId;


  }
  ngOnInit(): void {
    this.getDataForPaymentDetails();
    this.paymentnewform();
  }


  public paymentnewform(): void {
    this.paymentForm = this.fb.group({
      customerName: [this.orderDetails.name, [Validators.required]],
      orderNumber: [this.orderDetails.orderNumber, [Validators.required]],
      paymentReason: [1, Validators.required],
      creditCardTypeId: [null, [Validators.required]],
      creditCardNumber: ['', [Validators.required, Validators.maxLength(4)]],
      creditCardFeePercentage: ['', [Validators.required]],
      cardHolderName: ['', [Validators.required]],
      email: [this.orderDetails.email, [Validators.required, Validators.email]],
      creditCardPaymentAmount: [this.orderDetails.totalamount, [Validators.required]],
      creditCardFeeAmount: ['', Validators.required],
      totalAmount: ['', Validators.required],
      comments: [''],

    })

  }
  get formcontrol() {
    return this.paymentForm.controls;
  }
  public submit() {

  }
  public getDataForPaymentDetails() {
    this.spinner.show()
    try {
      this.spinner.hide()
      this.orderService.getDataForPaymentDetails().subscribe(res => {
        console.log(res)
        let data: any = res;
        this.creditCardTypes = data.creditCardTypes;
        this.paymentGatewayURL = data.paymentGatewayURL;
      }, err => { this.spinner.hide(); }, () => {
        this.spinner.hide();
      })
    } catch (error) {

    }
  }
  public credirCardChange() {
    let obj = this.creditCardTypes.find(x => { return x.creditCardTypeId == this.paymentForm.value.creditCardTypeId })
    console.log(obj);
    this.paymentForm.get('creditCardFeePercentage').setValue(obj ? obj.creditCardTypeFee : null)
    this.paymentForm.get('creditCardFeeAmount').setValue(obj ? (((+this.paymentForm.value.creditCardPaymentAmount / 100) * obj.creditCardTypeFee)).toFixed(2) : null)
    console.log(this.paymentForm.value.creditCardPaymentAmount)
    this.paymentForm.get('totalAmount').setValue(obj ? ((((+this.paymentForm.value.creditCardPaymentAmount / 100) * obj.creditCardTypeFee) + (+this.paymentForm.value.creditCardPaymentAmount))).toFixed(2) : null)
  }
  public saveOrderPayment() {
    this.formError = true;

    if (this.paymentForm.valid) {
      let param =
      {
        "OrderPaymentDetails":
        {

          "orderNumber": this.paymentForm.value.orderNumber,
          "ccTypeId": this.paymentForm.value.creditCardTypeId,
          "ccFeePerct": this.paymentForm.value.creditCardTypeId,
          "ccPayAmt": this.paymentForm.value.creditCardFeePercentage,
          "ccFeeAmt": this.paymentForm.value.creditCardFeeAmount,
          "totAmt": this.paymentForm.value.totalAmount,
          "comments": this.paymentForm.value.comments,
          "updatedUser": this.loginResponse.AuthenticateUser.userId,
          "paymentTypeId": this.paymentForm.value.paymentReason,
          "custEmail": this.paymentForm.value.email
        }
      }

      try {
        this.orderService.saveOrderPayment(param).subscribe(res => {
          console.log(res)
          let data: any = res;
          this.paymentId = data.orderPaymentId;

          setTimeout(() => {
            document.forms[0].action = this.paymentGatewayURL;
            document.forms[0].submit();
          }, 1000)


        })
      } catch (error) {

      }
    }
  }

  // onSubmit() {

  //   var body = "firstname=" + user.firstname + "&lastname=" + user.lastname + "&name=" + user.name;
  //   this.http.post("http://www.testtttt.com", body).subscribe((data) => { });

  // }


}
interface creditcardDetails {
  creditCardTypeFee: number,
  creditCardTypeId: number,
  creditCardTypeName: string
}