import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'text/plain',
  }),
};
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public webServiceUrl;
  public caterid = "";

  constructor(private http: HttpClient, private router: Router) {
    // this.getUrl();
  }

  getUrl() {
    this.webServiceUrl = localStorage.getItem('webServiceUrl');
    this.caterid = sessionStorage.getItem('catererId')
    // console.log(this.caterid.split("=")[1])
  }

  public login(userid, password): Observable<any> {
    this.getUrl();
    let obj={userId:userid,password:password,guestUserFlag:0};
    return this.http.post(`${this.webServiceUrl}/authenticateUser?catererId=${this.caterid}`,obj,httpOptions);
  
  }

  public logout(sessionId) {
    this.getUrl();
    return this.http.get(`${this.webServiceUrl}/logoutApplication?catererId=${this.caterid}&sessionId=${+sessionId}`);

  }
  public getCatererLogo() {
    this.getUrl();
    return this.http.get(`${this.webServiceUrl}/getCatererLogo?catererId=${this.caterid}`);

  }

  guestUser(): Observable<any> {
    this.getUrl();
    let obj={userId:null,password:null,guestUserFlag:1};
    return this.http.post(`${this.webServiceUrl}/authenticateUser?catererId=${this.caterid}`,obj,httpOptions);
  }

  redirectToHome() {
    if (JSON.parse(sessionStorage.getItem('templateValue')).templatevalue === 1) {
      this.router.navigate(['home']);
    } else {
      this.router.navigate(['cater-theme-one']);
    }
  }

  isuserLoggedin(): boolean {
    let data: any = JSON.parse(sessionStorage.getItem('loginResponse'))
    if (data) {
      // console.log(data)
      if (data.AuthenticateUser.userId != 0) {
        return true;
      } else {
        return false;
      }
    }
  }
  /**
   * caterlogo Method is used to get logo from branding details;
   */
  public caterlogoAvailble():boolean {
    let logo=sessionStorage.getItem('caterlogo');
    if(logo){
      return true;
    }else{
      return false;
    }
  }

  public random():boolean {
    let logo=sessionStorage.getItem('randomvalue');
    if(logo){
      return true;
    }else{
      return false;
    }
  }
  public caterlogo():string{
    // console.log('aaaaaaa')
    let logo=sessionStorage.getItem('caterlogo');
    if(logo){
      return logo;
    }else{
      // return false;
    }
  }
}
