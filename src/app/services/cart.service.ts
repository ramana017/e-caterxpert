import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  public webServiceUrl;
  public cart = {
    menuItems : [],
    package   : [],
    combo     : []
  };
  public subSectionId$ = new BehaviorSubject(null);

  public loginFromHome = new BehaviorSubject(null);
 public caterid='';


  constructor(private http: HttpClient){
    this.getUrl();
    let data=JSON.parse(sessionStorage.getItem('cartItems'));
    console.log(data)
    if(data){
      this.cart['menuItems'] = data.menuItems;
      this.cart['package'] = data.package;
      this.cart['combo'] = data.combo; 
    }
    
  }
  getUrl(){
    this.webServiceUrl = localStorage.getItem('webServiceUrl');
    this.caterid=sessionStorage.getItem('catererId')
    console.log(this.caterid);
  }
  getItems(): Observable<any>{
    this.getUrl();
    return this.http.get(`${this.webServiceUrl}/getItemsList?catererId=${this.caterid}`);
  }

  getPackage(id): Observable<any>{
    this.getUrl();
    return this.http.get(`${this.webServiceUrl}/showPackageDetails?itemId=${id}&catererId=${this.caterid}`);
  }

  getCombo(id): Observable<any>{
    this.getUrl();
    return this.http.get(`${this.webServiceUrl}/showComboDetails?itemId=${id}&catererId=${this.caterid}`);
  }


  increaseItemQuantity(itemId){
    this.cart.menuItems.map((x) => {
      if (x.itemId === itemId) {
        x.quantity++;
      }
    });
  }

  cartTotalAmount(){
    let Total = []
    this.cart.menuItems.forEach((x)=>{
      Total.push(+x.price * +x.quantity)
    });
    this.cart.package.forEach((x)=>{
      if (x.PackageDetails.packageItemLevelPriceFlag == 1) {
        Total.push(+x.PackageDetails.packageCost);
      } else {
        Total.push(+x.PackageDetails.PkgQty * +x.PackageDetails.packageCost);
      }
    });
    this.cart.combo.forEach((x)=>{
      Total.push(+x.ComboDetails.comboQty * +x.ComboDetails.comboCost)
    });
    let cartAmount = Total.reduce((a, b) => a + b, 0)
    return cartAmount;
  }

  clearCartItems(){
    this.cart.menuItems = [];
    this.cart.package = []
    this.cart.combo = [];
  }
}
