import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DatePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  public webServiceUrl;
  public caterid = '';

  httpOptions = {
    headers: new HttpHeaders({
      // 'Content-Type': 'application/json'
      'Content-Type': 'text/plain'

    })
  };

  constructor(private http: HttpClient) {
    this.getUrl()
  }
  getUrl() {
    this.webServiceUrl = localStorage.getItem('webServiceUrl');
    this.caterid = sessionStorage.getItem('catererId')
    console.log(this.caterid)


  }

  getPreviousOrders(userid): Observable<any> {
    this.getUrl();
    return this.http.get(`${this.webServiceUrl}/getMyOrderList?userId=${userid}&catererId=${this.caterid}`);
  }

  saveOrderDetails(data): Observable<any> {
    this.getUrl();
    return this.http.post(`${this.webServiceUrl}/saveOrderDetails?catererId=${this.caterid}`, JSON.stringify(data));
  }

  confirmOrder(orderId): Observable<any> {
    this.getUrl();
    return this.http.get(`${this.webServiceUrl}/confirmOrder?orderId=${orderId}&catererId=${this.caterid}`);
  }

  getCustomerAddress(customerId): Observable<any> {
    this.getUrl();
    return this.http.get(`${this.webServiceUrl}/getCustomerAddresses?customerId=${customerId}&catererId=${this.caterid}`);
  }

  getStateList(): Observable<any> {
    this.getUrl();
    return this.http.get(`${this.webServiceUrl}/getStateList?catererId=${this.caterid}`);
  }

  getAddressList(): Observable<any> {
    this.getUrl();
    return this.http.get(`${this.webServiceUrl}/getLocationAddress?catererId=${this.caterid}`);
  }

  repeatOrder(orderid): Observable<any> {
    this.getUrl();
    return this.http.get(`${this.webServiceUrl}/getRepeatOrderData?orderId=${orderid}&catererId=${this.caterid}`);
  }

  getTaxAmount(orderid): Observable<any> {
    this.getUrl();
    return this.http.get(`${this.webServiceUrl}/getOrderTaxes?orderId=${orderid}&catererId=${this.caterid}`);
  }

  getTemplate(): Observable<any> {
    this.getUrl();
    return this.http.get(`${this.webServiceUrl}/getTemplateValue?catererId=${this.caterid}`);
  }
  getDataForPaymentDetails(): Observable<any> {
    this.getUrl();
    return this.http.get(`${this.webServiceUrl}/getDataForPaymentDetails?catererId=${this.caterid}`)
  }
  saveOrderPayment(params): Observable<any> {
    this.getUrl();
    return this.http.post(`${this.webServiceUrl}/saveOrderPayment?catererId=${this.caterid}`, params, this.httpOptions)
  }

  updateOrderPaymentStatus(params) {
    this.getUrl();

    return this.http.post(`${this.webServiceUrl}/updateOrderPaymentStatus?catererId=${this.caterid}`, params, this.httpOptions)
  }
  public validateDiscountCode(discountCode, orderdate, customerId) {
    this.getUrl();
    return this.http.get(`${this.webServiceUrl}/validateDiscountCode?catererId=${this.caterid}&discountCode=${discountCode}&orderDate=${orderdate}&customerId=${customerId}`)
  }
  public validateItemAvailability(itemIds, orderdate, ordertime) {
    this.getUrl();
    return this.http.get(`${this.webServiceUrl}/validateItemAvailability?catererId=${this.caterid}&itemIds=${itemIds}&orderDate=${orderdate}&orderTime=${ordertime}`)

  }
  public getItemAvailability(itemId) {
    this.getUrl();
    return this.http.get(`${this.webServiceUrl}/getItemAvailability?catererId=${this.caterid}&itemId=${itemId}`)
  }
  public getBrandingDetails(value) {
    this.getUrl();
    return this.http.get(`${this.webServiceUrl}/getBrandingDetails?catererId=${value}`)
  }
public getDefaultDeliveryEndTime():Observable<any>{
  this.getUrl();
    return this.http.get(`${this.webServiceUrl}/getDefaultDeliveryEndTime?catererId=${this.caterid}`)
}
}

export function dateTransform(date) {
  // console.log(date)
  if (date !== null && date != undefined) {
    return new DatePipe('en-US').transform(date, 'MM/dd/yyyy hh:mm a');
  } else {
    return '';
  }
}
