import { Component, OnInit, DoCheck, OnDestroy } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ValidationAlertDialogComponent } from '../validation-alert-dialog/validation-alert-dialog.component';
declare var $: any;

@Component({
  selector: 'app-combo-dialog',
  templateUrl: './combo-dialog.component.html',
  styleUrls: ['./combo-dialog.component.scss']
})
export class ComboDialogComponent implements OnInit {
  public comboData;
  public comboData2;
  public comboQuantityTotal;
  public packNames = [];
  public itemsList = [];
  public showSummary = false;
  public distinctserviceName = [];//array to display distinct service names 
  summaryData;
  public product; // coming from component reciepe component as intial value
  constructor(private cartService: CartService, private modalRef: BsModalRef, private modalService: BsModalService) { }

  ngOnInit(): void {
    this.getComboItems(this.product.itemId);
  }

  /**
   * function : getComboItems
   * purpose  : this function used for list of combo items by passing product.itemId
   */

  public getComboItems(itemId): void {
    this.cartService.getCombo(itemId)
      .subscribe((data) => {
        console.log(data);
        this.comboData = data;
        this.comboData2 = JSON.parse(JSON.stringify(data));
        this.comboData.ComboDetails.comboQty = 1;
        this.comboQuantityTotal = this.comboData.ComboDetails.comboQty;
        this.removeDuplicate();
        this.assignQuantity();


        let comboItemsarray: Array<any> = this.comboData.ComboItems;
        comboItemsarray.map(x => {
          if (x.packageId == 0) {
            this.distinctserviceName = [...new Map(comboItemsarray.map(item =>
              [item['serviceName'], item])).values()];
          }
        })
        this.splitpacakges();
        // this.odrerBypackages();
        console.log(this.distinctserviceName);
      });
  }

  closeModal() {
    this.modalRef.hide();
  }
  /**
   * function : removeDuplicate
   * purpose  : this function used for ngFor to display the packageitems and menu items
   */

  public removeDuplicate() {
    let temp = new Map();
    let temp2 = [];
    for (let obj of this.comboData.ComboItems) {
      if (obj.packageId !== '0') {
        temp.set(obj.packName, obj);
      }
      if (obj.packageId === '0') {
        temp2.push(obj);
      }
    }
    this.packNames = [...temp.values()];
    this.itemsList = temp2;
    console.log(this.itemsList)
  }

  /**
   * function : assignQuantity
   * purpose : if the selected item already in the cart we are assigning the quantity on load
   */
  public assignQuantity() {
    if (this.cartService.cart.combo.length) {
      this.cartService.cart.combo.find(
        ({ ComboDetails, ComboItems }) => {
          if (ComboDetails.comboId === this.comboData.ComboDetails.comboId) {
            this.comboData.ComboDetails = JSON.parse(JSON.stringify(ComboDetails));
            this.comboData.ComboItems = JSON.parse(JSON.stringify(ComboItems));
            $(`#comboqtyid`).val(+this.comboData.ComboDetails.comboQty);
            this.comboQuantityTotal = $(`#comboqtyid`).val();
            this.comboData.ComboItems.forEach((x) => {
              if (x.packageId !== '0') {
                $(`#${x.itemId}`).val(x.itemQty);
              }
            });
          }
        }
      );
    }
  }

  public changeItemsQuantity(e) {
    this.comboQuantityTotal = +e.target.value;
    this.comboData2.ComboItems.forEach((x) => {
      // if package type equal to zero multiplying the quantity
      if (x.packType === '0' && x.packageId !== '0' && this.comboQuantityTotal > 0) {
        this.comboData.ComboItems.forEach(element => {
          if (x.itemId === element.itemId) {
            element['itemQty'] = +x.itemQty * this.comboQuantityTotal;
          }
        });
        // $(`#${x.itemId}`).val(+x.itemQty * this.comboQuantityTotal);
      }
    });
    this.comboData.ComboItems.forEach((x) => {
      this.comboData2.ComboItems.forEach(element => {
        if (x.packageId === '0' && element.packageId === '0' && x.itemId === element.itemId) {
          x['itemQty'] = +element.itemQty * this.comboQuantityTotal;
        }
      });

      // $(`#${x.itemId}someitem`).val(+x.itemQty * this.comboQuantityTotal);
    });


  }

  public onConfirm() {
    this.showSummary = false;
    this.takeOrder();
  }

  public onSave() {

    this.comboData.ComboDetails.comboQty = +$(`#comboqtyid`).val();
    this.comboData.ComboItems.forEach((x) => {
      // if packageid is not equal to zero it is package
      if (x.packageId !== '0') {
        x['itemQty'] = $(`#${x.itemId}`).val();
      }
      // if packageid is zero it is an item
      if (x.packageId === '0') {
        x['itemQty'] = $(`#${x.itemId}someitem`).val();
      }
    });

    // when there is no package items in combo directly accepting order
    if (!this.comboData.ComboItems.filter(x => x.packageId !== '0').length) {
      this.takeOrder();
    } else {
      // when combo contains package and list items also
      let temp = new Map();
      for (let obj of this.comboData.ComboItems.filter(x => x.itemQty > 0)) {
        if (obj.packageId !== '0') {
          temp.set(obj.packName, obj);
        }
      }
      this.summaryData = [...temp.values()];


      // classifying the selected items data into different arrays according to the packageName

      let group = this.comboData.ComboItems.reduce((acc, item) => {
        if (!acc[item.packName]) {
          if (item.packageId !== '0') {
            acc[item.packName] = [];
          }
        }
        if (item.packageId !== '0') {
          acc[item.packName].push(item);
        }
        return acc;
      }, {});

      let itemsForValidation = [];
      itemsForValidation.push(Object.values(group));
      console.log(itemsForValidation);

      // validating the data onsubmit
      let result = [];
      itemsForValidation.forEach((x, i) => {
        x.forEach(item => {
          // if packtype one
          if (item[0].packType === "1" && (item[0].maxItems !== '0' && item[0].minItems !== '0')) {
            if (item.filter(y => +y.itemQty > 0).length >= +item[0].minItems &&
              item.filter(y => +y.itemQty > 0).length <= +item[0].maxItems &&
              item.reduce((a, b) => +a + +b.itemQty, 0) == +this.comboData.ComboDetails.comboQty * item[0].maxItemQty
            ) {
              result.push(true);
            } else {
              this.modalService.show(ValidationAlertDialogComponent, {
                class: 'modal-dialog-custom ',
                initialState: { message: `For  Package- ${item[0].packName.bold()} <br> The Total item quantity should be  equal to  ${this.comboQuantityTotal * item[0].maxItemQty}.` },
                keyboard: false,
              });
              result.push(false);
              // alert(`For  Package-${item[0].packName} \n The Total item quantity should be  equal to  ${this.comboQuantityTotal}.`)
            }
          }
          // if packtype not equal to one
          if (item[0].packType === "1" && (item[0].maxItems === '0' && item[0].minItems === '0')) {
            if (item.reduce((a, b) => +a + +b.itemQty, 0) == +this.comboData.ComboDetails.comboQty * item[0].maxItemQty) {
              result.push(true);
            } else {
              this.modalService.show(ValidationAlertDialogComponent, {
                class: 'modal-dialog-custom ',
                initialState: { message: `For  Package- ${item[0].packName.bold()} <br> The Total item quantity should be equal to  ${this.comboQuantityTotal * item[0].maxItemQty}.` },
                keyboard: false,
              });
              result.push(false);
              // alert(`For  Package-${item[0].packName} \n The Total item quantity should be equal to  ${this.comboQuantityTotal}.`)
            }
          }

          // if packtype not equal to zero
          if (item[0].packType === "0") {
            result.push(true);
          }

        });
      });

      if (result.includes(false)) {
        this.showSummary = false;
        console.log('err');
      } else {
        this.showSummary = true;
        // console.log('take order');
        // this.takeOrder();
      }

    }

  }




  public takeOrder() {

    this.cartService.cart.combo.forEach((x, i) => {
      if (this.comboData.ComboDetails.comboId === x.ComboDetails.comboId) {
        this.cartService.cart.combo[i] = this.comboData;
      }
    });
    const productExistInCart = this.cartService.cart.combo.find(
      ({ ComboDetails }) => ComboDetails.comboId === this.comboData.ComboDetails.comboId);

    if (!productExistInCart) {
      this.cartService.cart.combo.push(this.comboData);
    }
    this.modalRef.hide();
  }

  public comboContentDisplayOrder = [];
  public comboContentNoPackegeId = [];


  private splitpacakges(): void {
    let comboContentDisplayArr: Array<any>;
    let comboItemsarray: Array<any> = this.comboData.ComboItems;
    comboItemsarray.map(x => {
      comboContentDisplayArr = [...new Map(comboItemsarray.map(item =>
        [item['comboContentDisplayArr'], item])).values()];

    })
    console.log(comboContentDisplayArr);
    comboContentDisplayArr.map(x => {
      let package2arr = [];
      let comboContentDisplayArr = x;
      let packagename = x.packageId == 0 ? x.serviceName : x.packName
      comboItemsarray.map(y => {

        if (x.packageId == y.packageId) {
          if (y.packageId == 0) {
            if (x.comboContentDisplayArr == y.comboContentDisplayArr) {
              this.comboContentNoPackegeId.push(y);
            }
          } else {
            package2arr.push(y);

          }
        }

      })
      let obj = { "packagedetails": comboContentDisplayArr, "packName": packagename, data: package2arr }
      package2arr.length > 0 ? this.comboContentDisplayOrder.push(obj) : '';
    })
    this.comboContentNoPackegeId.map(x => {
      if (x.packageId == 0) {
        this.distinctserviceName = [...new Map(comboItemsarray.map(item =>
          [item['serviceName'], item])).values()];
      }
    })
    this.comboContentDisplayOrder=this.comboContentDisplayOrder.sort((a,b)=>a.packagedetails.comboContentDisplayArr-b.packagedetails.comboContentDisplayArr)


    console.log(this.comboContentDisplayOrder, this.comboContentNoPackegeId, this.distinctserviceName)




  }

  private odrerBypackages() {
    let comboItemsarray: Array<any> = this.comboData.ComboItems;
    let sortedarray = comboItemsarray.sort((a, b) => a.comboContentDisplayArr - b.comboContentDisplayArr);
    console.log("sorted array", sortedarray)
    let uniquecomboContentDisplayArr = sortedarray.map(item => item.comboContentDisplayArr)
      .filter((value, index, self) => self.indexOf(value) === index)


    let totalpackagesArray = [];
    let pusheddisplayArray = [];
    sortedarray.map(x => {
      let individualPackage = [];
      let i = 0;
      comboItemsarray.map(y => {
        if (x.comboContentDisplayArr == y.comboContentDisplayArr) {
          individualPackage.push(x);
          comboItemsarray.splice(i, 0)

        }
        
      })
      let obj = { 'diplayarr': x.comboContentDisplayArr, data: individualPackage }
      pusheddisplayArray.includes(x.comboContentDisplayArr) ? '' : totalpackagesArray.push(obj);
    })
    console.log(totalpackagesArray);

    console.log(uniquecomboContentDisplayArr);

    let finalarray = [];
    let i = 0;
    totalpackagesArray.map(x => {
      if (uniquecomboContentDisplayArr.includes(x.diplayarr)) {
        finalarray.push(x)
        uniquecomboContentDisplayArr.splice(i, 1);
      }
    }
    )
    console.log(finalarray)
  }

}
