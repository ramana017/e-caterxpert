import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrdersComponent } from './components/orders/orders.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { OrderDetailsComponent } from './components/order-details/order-details.component';
import { MyOrdersListComponent } from './components/my-orders-list/my-orders-list.component';
import { PlaceOrderComponent } from './components/place-order/place-order.component';
import { OrderConfirmationComponent } from './components/order-confirmation/order-confirmation.component';
import { TemplateGuard } from './shared/guards/template.guard';
import { AppComponent } from './app.component';
import { PaymentDetailsComponent } from './components/payment-details/payment-details.component';
import { PayementresponseComponent } from './payementresponse/payementresponse.component';
import { ComboPopupsComponent } from './components/combo-popups/combo-popups.component';
import { CaterThemeOneItemsComponent } from './components/cater-theme-one-items/cater-theme-one-items.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { RecipeComponent } from './components/recipe/recipe.component';


const routes: Routes = [
  {path: '', component: AppComponent},
  {path: 'home', component: RecipeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'orders', component: OrdersComponent},
  {path: 'order-details/:isBack', component: OrderDetailsComponent},
  {path: 'order-details', component: OrderDetailsComponent},

  {path: 'orders-list', component: MyOrdersListComponent},
  {path: 'place-order', component: PlaceOrderComponent},
  {path: 'order-confirmation', component: OrderConfirmationComponent},
  {path: 'cater-theme-one',  component: CaterThemeOneItemsComponent},
  {path: 'payment-details',  component: PaymentDetailsComponent},
  {path: 'payment-details/:tid',  component: PaymentDetailsComponent},

  {path: 'payment-response/:successflag',  component: PayementresponseComponent},
  {path:'combo', component: ComboPopupsComponent},
  {path:'pnf',component:PageNotFoundComponent},


  {path: '**', redirectTo: 'home'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
