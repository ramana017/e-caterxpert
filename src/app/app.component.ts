import { Component, OnInit ,AfterViewInit} from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { CartService } from './services/cart.service';
import { OrderService } from './services/order.service';
import { HostListener } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit ,AfterViewInit{
  title = 'E-Catering';
  public baseUrl = 'assets/url.json';
  public res;

  @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
    this.processData();
  }

  constructor(private router: Router,
    private http: HttpClient,
    private cartService: CartService,
    private orderService: OrderService,
    public authservice: AuthService) { }
  ngOnInit(): void {
   
  }
  ngAfterViewInit(){
    this.http.get(this.baseUrl).subscribe((res) => {
      this.res = res;
      sessionStorage.setItem('captchaKey', this.res.captchaKey);
      let webService = this.res.webserviceUrl;
      localStorage.setItem('webServiceUrl', webService);
      this.cartService.getUrl();
      // this.gettemp()
    this.getBrandingDetails();

    });
  }

  gettemp(templatevalue) {
    console.log(templatevalue)
    let template = sessionStorage.getItem('templateValue');
    if (template) {
    } else {
      sessionStorage.setItem('templateValue', JSON.stringify(templatevalue));
      if (templatevalue === 1) {
        this.router.navigate(['home']);
      } else {
        this.router.navigate(['cater-theme-one']);
      }


    }

  }
  private themeWrapper = document.querySelector('body');


  getBrandingDetails() {
    let value: string = sessionStorage.getItem('randomvalue');
    console.log("Branding details")
    try {
      this.orderService.getBrandingDetails(value.split('=')[1]).subscribe(res => {
        console.log(res);
        let style: any = res;
        sessionStorage.setItem('catererId', style.brandingDetails[0].catererId);
        if (style.brandingDetails[0].blobcontent.length > 1) {
          console.log(style.brandingDetails[0].blobcontent > 1)
          sessionStorage.setItem('caterlogo', style.brandingDetails[0].blobcontent)
        }
        this.gettemp(+(style.brandingDetails[0].templateValue))
        if (style.brandingDetails[0].active == "1") {
          this.themeWrapper.style.setProperty('--buttonColor', style.brandingDetails[0].buttons);
          this.themeWrapper.style.setProperty('--applicationHeaderColor', style.brandingDetails[0].applicationHeader);
          this.themeWrapper.style.setProperty('--sectionHeaderColor', style.brandingDetails[0].sectionHeader);
          // this.themeWrapper.style.setProperty('--fontfamily', style.brandingDetails[0].fontfamily);
        }
        else {
          this.themeWrapper.style.setProperty('--buttonColor', '#262262');
          this.themeWrapper.style.setProperty('--applicationHeaderColor', '#dcdcdc');
          this.themeWrapper.style.setProperty('--sectionHeaderColor', '#262262');
          // let font="Montserrat-Regular"
          // this.themeWrapper.style.setProperty('--fontfamily',font);          
        }
        this.cartService.getUrl();
        this.orderService.getUrl();
        this.authservice.getUrl();

      }, err => {
        console.log(err);
        sessionStorage.removeItem('randomvalue')
        this.router.navigateByUrl('pnf')
      })

    } catch (error) {
      console.log(error)
    }
  }
  processData() {

    // store data into local storage before browser refresh
    sessionStorage.removeItem('caterlogo')
    sessionStorage.setItem('cartItems', JSON.stringify(this.cartService.cart))
  }

}
